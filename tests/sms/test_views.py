import json

from app.app import (
    api,
    db
)
from app.auth.models import Client
from tests.constants import CLI_CLIENT_AUTHORIZATION_HEADERS
from tests.auth.fixtures import cli_client_fixture

prefix = api.prefix


def test_sms_get_401(client):
    response = client.get(f'{prefix}/sms')
    assert response.status == '401 UNAUTHORIZED'


def test_sms_get_200(
    client,
    cli_client_fixture
):
    c_client = Client(**cli_client_fixture)
    db.session.add(c_client)
    db.session.commit()
    response = client.post(
        '/oauth/token',
        data={'grant_type': 'client_credentials'},
        headers=CLI_CLIENT_AUTHORIZATION_HEADERS
    )
    token = json.loads(response.data).get('access_token')

    response = client.get(
        f'{prefix}/sms',
        headers={'Authorization': f'Bearer {token}'}
    )
    assert response.status == '200 OK'


def test_sms_sending(
    client,
    cli_client_fixture
):
    sms = {'phone': '+380775543765', 'message': 'hello world'}
    c_client = Client(**cli_client_fixture)
    db.session.add(c_client)
    db.session.commit()
    response = client.post(
        '/oauth/token',
        data={'grant_type': 'client_credentials'},
        headers=CLI_CLIENT_AUTHORIZATION_HEADERS
    )
    token = json.loads(response.data).get('access_token')

    response = client.post(
        f'{prefix}/sms',
        json=sms,
        headers={'Authorization': f'Bearer {token}'}
    )

    assert response.status == '200 OK'
    assert json.loads(response.data) == sms
