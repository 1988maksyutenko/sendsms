from base64 import b64encode

CLI_CLIENT_ID = 'qwer'
CLI_CLIENT_SECRET = 'qwer_qwer'
CLI_CLIENT_B64 = b64encode(
    str.encode(f'{CLI_CLIENT_ID}:{CLI_CLIENT_SECRET}')
).decode('utf-8')
CLI_CLIENT_AUTHORIZATION_HEADERS = {
    'Authorization': f'Basic {CLI_CLIENT_B64}'
}
