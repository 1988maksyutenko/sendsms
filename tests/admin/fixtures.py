import pytest


@pytest.fixture
def user_1_fixture():
    return {
        'id': 2,
        'email': 'test1-email@mail.com',
        'password': '0p9o8i7u6y5t',
        'first_name': 'user1',
        'last_name': 'new-user',
        'middle_name': 'middle',
        'position_id': 2,
        'is_active': True
    }



@pytest.fixture
def user_2_fixture():
    return {
        'id': 3,
        'email': 'test2-email@mail.com',
        'password': '0p9o8i7u6y5t',
        'first_name': 'user1',
        'last_name': 'new-user',
        'middle_name': 'middle',
        'position_id': 3,
        'is_active': True
    }


@pytest.fixture
def user_3_fixture():
    return {
        'id': 4,
        'email': 'test3-email@mail.com',
        'password': '0p9o8i7u6y5t',
        'first_name': 'user1',
        'last_name': 'new-user',
        'middle_name': 'middle',
        'position_id': 3,
        'is_active': True
    }