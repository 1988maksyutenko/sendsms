from app.admin.services.errors import (
    UserPositionDoesNotExist,
    UserPositionAlreadyExist
)


def test_user_position_does_not_exist(client):
    err = UserPositionDoesNotExist()
    assert str(err) == err.message
    assert str(err) == repr(err)


def test_user_positin_already_exist(client):
    err = UserPositionAlreadyExist()
    assert str(err) == err.message
    assert str(err) == repr(err)
