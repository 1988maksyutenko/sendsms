from app.admin.services import TokenHandler
from app.auth.models import Token

from tests.auth.fixtures import (
    token_fixture,
    cli_client_fixture
)
from tests.auth.helpers import (
    create_test_token,
    create_test_client
)


def test_token_handler_creation(
    client,
    token_fixture,
    cli_client_fixture
):
    create_test_client(cli_client_fixture)
    token = create_test_token(token_fixture)
    handler = TokenHandler(token)
    handler_from_id = TokenHandler.from_token_id(token_fixture.get('id'))

    assert handler.token == token
    assert handler_from_id.token == token


def test_delete_token(
    client,
    token_fixture,
    cli_client_fixture
):
    create_test_client(cli_client_fixture)
    token = create_test_token(token_fixture)
    handler = TokenHandler.from_token_id(token_fixture.get('id'))

    deleted_token = handler.delete_token()

    assert deleted_token == token
    assert not Token.query.filter_by(id=token_fixture.get('id')).first()
