import re
import json
import pytest

from app.auth.models import (
    User,
    UserPosition,
    Token,
    Client
)
from tests.admin.fixtures import (
    user_1_fixture,
    user_2_fixture,
    user_3_fixture
)
from tests.auth.fixtures import (
    admin_fixture,
    user_fixture,
    user_positions_fixture,
    cli_client_fixture,
    token_fixture
)
from tests.auth.helpers import (
    create_test_user,
    create_test_user_position,
    create_test_client,
    create_test_token
)
from tests.admin.helpers import get_admin_session_cookie


def test_admin_page(
    client,
    admin_fixture,
    user_positions_fixture
):
    response = client.get('/admin-page')
    assert response.status == '401 UNAUTHORIZED'

    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )
    response = client.get(
        '/admin-page',
        headers={'Cookie': cookies}
    )
    assert response.status == '200 OK'


def test_create_user(
    client,
    admin_fixture,
    user_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user_position(user_positions_fixture['manager'])
    create_test_user(admin_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.get('/create-user', headers={'Cookies': cookies})
    assert response.status == '200 OK'

    response = client.post(
        '/create-user',
        data={
            'email': user_fixture['email'],
            'password': user_fixture['password'],
            'user_position': user_positions_fixture['manager']['name'],
            'first_name': user_fixture['first_name'],
            'last_name': user_fixture['last_name'],
            'middle_name': user_fixture['middle_name'],
        },
        headers={'Cookies': cookies}
    )

    user = User.query.filter_by(id=1).first()
    assert response.status == '201 CREATED'
    assert user.get_user_id() == 1


def test_delete_user(
    client,
    admin_fixture,
    user_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    create_test_user_position(user_positions_fixture['manager'])
    create_test_user(user_fixture)

    response = client.post(
        f'/delete-user/{user_fixture.get("id")}',
        headers={'Cookies': cookies}
    )
    assert response.status == '200 OK'
    assert User.query.filter_by(id=user_fixture.get('id')).first() is None

    response = client.post(
        '/delete-user/33',
        headers={'Cookies': cookies}
    )
    assert response.status == '400 BAD REQUEST'


def test_create_user_already_exists(
    client,
    admin_fixture,
    user_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    create_test_user_position(user_positions_fixture['manager'])
    create_test_user(user_fixture)
    response = client.post(
        '/create-user',
        data={
            'email': user_fixture['email'],
            'password': user_fixture['password'],
            'user_position': user_positions_fixture['manager']['name'],
            'first_name': user_fixture['first_name'],
            'last_name': user_fixture['last_name'],
            'middle_name': user_fixture['middle_name']
        },
        headers={'Cookies': cookies}
    )

    assert response.status == '400 BAD REQUEST'


def test_list_all_users(
    client,
    admin_fixture,
    user_positions_fixture,
    user_1_fixture,
    user_2_fixture,
    user_3_fixture
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user_position(user_positions_fixture['manager'])
    create_test_user_position(user_positions_fixture['developer'])
    create_test_user(admin_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )
    create_test_user(user_1_fixture)
    create_test_user(user_2_fixture)
    create_test_user(user_3_fixture)

    response = client.get(
        '/list-users',
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'

    user = User.query.filter_by(id=user_1_fixture.get('id')).first()
    assert user.get_user_id() == user_1_fixture.get('id')

    user = User.query.filter_by(id=user_2_fixture.get('id')).first()
    assert user.get_user_id() == user_2_fixture.get('id')

    user = User.query.filter_by(id=user_3_fixture.get('id')).first()
    assert user.get_user_id() == user_3_fixture.get('id')


def test_get_particular_user(
    client,
    admin_fixture,
    user_positions_fixture,
    user_fixture,
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user_position(user_positions_fixture['manager'])
    create_test_user(admin_fixture)
    create_test_user(user_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )
    response = client.get(
        f"/user/{admin_fixture.get('id')}",
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'
    assert re.search('email', str(response.data))


def test_update_particular_user(
    client,
    admin_fixture,
    user_positions_fixture,
    user_fixture,
    user_1_fixture,
):
    new_email = 'newtestemail@amil.com'
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user_position(user_positions_fixture['manager'])
    create_test_user(admin_fixture)
    create_test_user(user_fixture)
    create_test_user(user_1_fixture)
    id = user_fixture.pop('id')
    user_fixture.pop('is_active')
    user_fixture['email'] = new_email
    user_fixture.setdefault('user_position', 'manager')
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )
    response = client.post(
        f"/user/{id}",
        data=user_fixture,
        headers={'Cookies': cookies}
    )
    user = User.query.filter_by(id=id).first()

    assert response.status == '200 OK'
    assert user.email == new_email

    user_fixture['email'] = user_1_fixture.get('email')
    response = client.post(
        f"/user/{id}",
        data=user_fixture,
        headers={'Cookies': cookies}
    )
    assert response.status == '400 BAD REQUEST'


def test_get_all_user_positions(
    client,
    admin_fixture,
    user_positions_fixture,
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.get(
        '/list-positions',
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'
    assert re.search('admin', str(response.data))


def test_get_particular_position(
    client,
    admin_fixture,
    user_positions_fixture,
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.get(
        f"/position/{user_positions_fixture.get('admin').get('id')}",
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'
    assert re.search('admin', str(response.data))


def test_create_user_position_get(
    client,
    admin_fixture,
    user_positions_fixture,
):
    create_test_user_position(user_positions_fixture.get('admin'))
    create_test_user(admin_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.get('/create-position', headers={'Cookies': cookies})
    assert response.status == '200 OK'


def test_create_user_position(
    client,
    admin_fixture,
    user_positions_fixture,
):
    create_test_user_position(user_positions_fixture.get('admin'))
    create_test_user(admin_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    manager_position = user_positions_fixture.get('manager')
    manager_position['description'] = ''
    response = client.post(
        '/create-position',
        data=manager_position,
        headers={'Cookies': cookies}
    )

    assert response.status == '201 CREATED'
    assert manager_position.get('name') == UserPosition.query.filter_by(
        id=1
    ).first().name

    response = client.post(
        '/create-position',
        data=manager_position,
        headers={'Cookies': cookies}
    )

    assert response.status == '400 BAD REQUEST'


def test_update_particular_position(
    client,
    admin_fixture,
    user_positions_fixture,
):
    new_position = 'top manager'
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user_position(user_positions_fixture['manager'])
    manager_position_id = user_positions_fixture.get('manager').get('id')
    create_test_user(admin_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.post(
        f'position/{manager_position_id}',
        data={'name': new_position, 'description': ''},
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'
    assert new_position == UserPosition.query.filter_by(
        id=manager_position_id
    ).first().name

    response = client.post(
        f'position/{manager_position_id}',
        data={'name': 'admin', 'description': ''},
        headers={'Cookies': cookies}
    )
    assert response.status == '400 BAD REQUEST'


def test_position_deletion(
    client,
    admin_fixture,
    user_positions_fixture,
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user_position(user_positions_fixture['manager'])
    position_id = user_positions_fixture.get('manager').get('id')
    create_test_user(admin_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.post(
        f'/delete-position/{position_id}',
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'
    assert not UserPosition.query.filter_by(
        id=user_positions_fixture.get('manager').get('id')
    ).first()

    response = client.post(
        f'/delete-position/{position_id}',
        headers={'Cookies': cookies}
    )
    assert response.status == '400 BAD REQUEST'


def test_get_all_tokens(
    client,
    admin_fixture,
    user_positions_fixture,
    cli_client_fixture,
    token_fixture
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    create_test_client(cli_client_fixture)
    create_test_token(token_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.get(
        '/tokens',
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'
    assert re.search(
        token_fixture.get('access_token'),
        str(response.data)
    )
    assert re.search(
        cli_client_fixture.get('name'),
        str(response.data)
    )


def test_retrieving_particular_token(
    client,
    admin_fixture,
    user_positions_fixture,
    cli_client_fixture,
    token_fixture
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    create_test_client(cli_client_fixture)
    create_test_token(token_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.get(
        '/token/1',
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'
    assert re.search(
        token_fixture.get('access_token'),
        str(response.data)
    )


def test_sdeleting_particular_token(
    client,
    admin_fixture,
    user_positions_fixture,
    cli_client_fixture,
    token_fixture
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    create_test_client(cli_client_fixture)
    create_test_token(token_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.post(
        '/token/1',
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'
    assert re.search(
        'deleted',
        str(response.data)
    )
    assert re.search(
        token_fixture.get('access_token'),
        str(response.data)
    )
    assert not Token.query.filter_by(id=token_fixture.get('id')).first()


def test_get_all_clients(
    client,
    admin_fixture,
    user_positions_fixture,
    cli_client_fixture,
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    create_test_client(cli_client_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.get(
        '/clients',
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'
    assert re.search(
        cli_client_fixture.get('client_id'),
        str(response.data)
    )


def test_get_particular_client(
    client,
    admin_fixture,
    user_positions_fixture,
    cli_client_fixture,
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    create_test_client(cli_client_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.get(
        '/client/1',
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'
    assert re.search(
        cli_client_fixture.get('client_id'),
        str(response.data)
    )


def test_update_client(
    client,
    admin_fixture,
    user_positions_fixture,
    cli_client_fixture,
):
    new_client_id = 'new id'
    new_client_secret = 'new client secret'
    new_client_metadata = '{"data": "test"}'
    new_client_name = 'new name'
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    create_test_client(cli_client_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    cli_client_fixture.pop('id')
    cli_client_fixture.pop('client_id_issued_at')
    cli_client_fixture.pop('client_secret_expires_at')
    cli_client_fixture['user_id'] = admin_fixture.get('id')
    cli_client_fixture['name'] = new_client_name
    cli_client_fixture['client_id'] = new_client_id
    cli_client_fixture['client_secret'] = new_client_secret
    cli_client_fixture['client_metadata'] = new_client_metadata

    response = client.post(
        '/client/1',
        data=cli_client_fixture,
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'
    assert re.search('client was updated', str(response.data))


def test_create_new_client(
    client,
    admin_fixture,
    user_positions_fixture,
    cli_client_fixture,
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.get(
        '/create-client',
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'

    response = client.post(
        'create-client',
        data={
            'client_id': cli_client_fixture.get('client_id'),
            'client_secret': cli_client_fixture.get('client_secret'),
            'client_metadata': cli_client_fixture.get('_client_metadata'),
            'name': cli_client_fixture.get('name'),
            'user_id': admin_fixture.get('id')
        },
        headers={'Cookies': cookies}
    )

    created_client = Client.query.filter_by(
        client_id=cli_client_fixture.get('client_id')
    ).first()
    assert response.status == '201 CREATED'
    assert created_client.client_id == cli_client_fixture.get('client_id')
    assert (
        created_client.client_secret == cli_client_fixture.get('client_secret')
    )
    assert (
        created_client.client_metadata ==
        json.loads(cli_client_fixture.get('_client_metadata'))
    )


def test_create_client_errors(
    client,
    admin_fixture,
    user_positions_fixture,
    cli_client_fixture,
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    create_test_client(cli_client_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.post(
        'create-client',
        data={
            'client_secret': cli_client_fixture.get('client_secret'),
            'client_metadata': cli_client_fixture.get('_client_metadata'),
            'name': cli_client_fixture.get('name'),
        },
        headers={'Cookies': cookies}
    )

    assert response.status == '400 BAD REQUEST'

    response = client.post(
        'create-client',
        data={
            'client_id': cli_client_fixture.get('client_id'),
            'client_secret': cli_client_fixture.get('client_secret'),
            'client_metadata': cli_client_fixture.get('_client_metadata'),
            'name': cli_client_fixture.get('name'),
            'user_id': admin_fixture.get('id')
        },
        headers={'Cookies': cookies}
    )

    assert response.status == '400 BAD REQUEST'
    assert re.search('exists', str(response.data))


def test_client_deletion(
    client,
    admin_fixture,
    user_positions_fixture,
    cli_client_fixture,
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    create_test_client(cli_client_fixture)
    cookies = get_admin_session_cookie(
        client,
        admin_fixture.get('email'),
        admin_fixture.get('password')
    )

    response = client.post(
        f'/delete-client/{cli_client_fixture.get("id")}',
        headers={'Cookies': cookies}
    )

    assert response.status == '200 OK'
    assert re.search('deleted:', str(response.data))

    response = client.post(
        '/delete-client/33',
        headers={'Cookies': cookies}
    )

    assert response.status == '400 BAD REQUEST'
