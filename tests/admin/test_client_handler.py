import pytest
import json

from werkzeug.datastructures import MultiDict
from wtforms.validators import ValidationError

from tests.auth.fixtures import (
    cli_client_fixture,
    user_fixture,
    user_positions_fixture
)
from tests.auth.helpers import (
    create_test_client,
    create_test_user,
    create_test_user_position
)
from app.admin.services.errors import (
    ClientDoesNotExists,
    ClientAlreadyExists
)
from app.admin.services import ClientHandler
from app.auth.models import Client


def test_client_handler_creation(
    client,
    cli_client_fixture
):
    client = create_test_client(cli_client_fixture)
    handler = ClientHandler(client)
    from_id_handler = ClientHandler.from_client_id(client.id)
    cli_client_fixture.pop('id')
    cli_client_fixture.pop('client_id_issued_at')
    cli_client_fixture.pop('client_secret_expires_at')
    cli_client_fixture['user_id'] = 21
    form = MultiDict(cli_client_fixture)
    from_form_handler = ClientHandler.from_form(form)
    handler.form = form

    assert client == handler.client
    assert client == from_id_handler.client
    assert client == from_form_handler.client
    assert form.get('client_id') == from_form_handler.form.get('client_id')
    assert form.get('client_id') == handler.form.get('client_id')


def test_client_handler_errors(
    client,
    cli_client_fixture
):
    client = create_test_client(cli_client_fixture)
    cli_client_fixture.pop('id')
    cli_client_fixture.pop('client_id_issued_at')
    cli_client_fixture.pop('client_secret_expires_at')
    cli_client_fixture.pop('client_id')
    form = MultiDict(cli_client_fixture)

    with pytest.raises(ClientDoesNotExists):
        ClientHandler.from_client_id(1234)
    with pytest.raises(ValidationError):
        ClientHandler.from_form(form)
    with pytest.raises(ValidationError):
        handler = ClientHandler(client)
        handler.form = form


def test_update_client(
    client,
    cli_client_fixture,
    user_fixture,
    user_positions_fixture
):
    new_client_id = 'new id'
    new_client_secret = 'new client secret'
    new_client_metadata = '{"data": "test"}'
    new_client_name = 'new name'
    client = create_test_client(cli_client_fixture)
    create_test_user_position(user_positions_fixture.get('manager'))
    create_test_user(user_fixture)

    cli_client_fixture.pop('id')
    cli_client_fixture.pop('client_id_issued_at')
    cli_client_fixture.pop('client_secret_expires_at')
    cli_client_fixture['user_id'] = user_fixture.get('id')
    cli_client_fixture['name'] = new_client_name
    cli_client_fixture['client_id'] = new_client_id
    cli_client_fixture['client_secret'] = new_client_secret
    cli_client_fixture['client_metadata'] = new_client_metadata
    form = MultiDict(cli_client_fixture)

    handler = ClientHandler.from_client_id(client.id)
    handler.form = form
    updated_client = handler.update_client()

    assert updated_client.client_id == new_client_id
    assert updated_client.client_secret == new_client_secret
    assert updated_client.client_metadata == json.loads(new_client_metadata)
    assert updated_client.name == new_client_name
    assert updated_client.user_id == user_fixture.get('id')


def test_create_new_client(
    client,
    cli_client_fixture,
    user_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture.get('manager'))
    create_test_user(user_fixture)
    cli_client_fixture.pop('id')
    cli_client_fixture.pop('client_id_issued_at')
    cli_client_fixture.pop('client_secret_expires_at')
    cli_client_fixture['user_id'] = user_fixture.get('id')
    form = MultiDict(cli_client_fixture)
    handler = ClientHandler.from_form(form)
    new_client = handler.create_new_client()

    assert new_client.client_id == cli_client_fixture.get('client_id')
    assert new_client.client_secret == cli_client_fixture.get('client_secret')
    assert (
        new_client.client_metadata ==
        json.loads(cli_client_fixture.get('_client_metadata'))
    )

    with pytest.raises(ClientAlreadyExists):
        form.setdefault('_client_metadata', {})
        handler = ClientHandler.from_form(form)
        handler.create_new_client()


def test_client_deletion(
    client,
    cli_client_fixture
):
    client = create_test_client(cli_client_fixture)
    handler = ClientHandler(client)
    deleted_client = handler.delete_client()

    assert client == deleted_client
    assert not Client.query.filter_by(client_id=client.client_id).first()
