import pytest

from werkzeug.datastructures import MultiDict

from app.auth.models import User
from app.admin.services import UserHandler
from app.admin.services.errors import (
    UserAlreadyExists,
    UserDoesNotExists
)

from tests.admin.fixtures import (
    user_1_fixture,
    user_2_fixture
)
from tests.auth.helpers import (
    create_test_user,
    create_test_user_position
)
from tests.auth.fixtures import user_positions_fixture
from wtforms.validators import ValidationError


def test_user_handler_form_setter_exception(
    client,
    user_1_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture['manager'])
    create_test_user(user_1_fixture)
    handler = UserHandler.from_user_id(user_1_fixture.get('id'))

    with pytest.raises(ValidationError):
        user_1_fixture.pop('id')
        user_1_fixture.pop('is_active')
        user_1_fixture.pop('email')
        handler.form = MultiDict(user_1_fixture)


def test_user_handler_from_form_initialization(
    client,
    user_1_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture['manager'])
    user = create_test_user(user_1_fixture)
    user_1_fixture.pop('id')
    user_1_fixture.pop('is_active')
    user_1_fixture.pop('position_id')
    user_1_fixture.setdefault('user_position', 'manager')
    handler = UserHandler.from_form(MultiDict(user_1_fixture))
    assert handler.user == user

    error_data = MultiDict(
        {
            'email': 'somemail.com',
            'password': '',
            'first_name': '',
            'last_name': '',
            'middle_name': '',
            'user_position': ''
        }
    )
    with pytest.raises(ValidationError):
        UserHandler.from_form(error_data)

    error_data['user_position'] = 'manager'
    with pytest.raises(ValidationError):
        UserHandler.from_form(error_data)

    handler = UserHandler.from_user_id(user.id)
    assert handler.user == user
    with pytest.raises(UserDoesNotExists):
        handler = UserHandler.from_user_id(2345)


def test_user_handler_create_new_user(
    client,
    user_1_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture['manager'])

    user_1_fixture.pop('id')
    user_1_fixture.pop('is_active')
    user_1_fixture.setdefault('user_position', 'manager')
    handler = UserHandler.from_form(MultiDict(user_1_fixture))
    new_user = handler.create_new_user()
    assert isinstance(new_user, User)
    assert new_user == User.query.filter_by(id=1).first()

    with pytest.raises(UserAlreadyExists):
        handler = UserHandler.from_form(MultiDict(user_1_fixture))
        new_user = handler.create_new_user()


def test_user_handler_delete_user(
    client,
    user_1_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture['manager'])
    create_test_user(user_1_fixture)
    handler = UserHandler.from_user_id(user_1_fixture.get('id'))
    deleted_user = handler.delete_user()
    assert deleted_user == handler.user


def test_user_handler_get_current_user(
    client,
    user_1_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture['manager'])
    user = create_test_user(user_1_fixture)

    handler = UserHandler.from_user_id(user_1_fixture.get('id'))
    required_user = handler.get_current_user()
    assert required_user == user


def test_user_handler_update_user(
    client,
    user_1_fixture,
    user_2_fixture,
    user_positions_fixture
):
    new_email = 'newtestemail@mail.com'
    new_last_name = 'new last name'
    create_test_user_position(user_positions_fixture['manager'])
    create_test_user(user_1_fixture)

    id = user_1_fixture.pop('id')
    user_1_fixture.pop('is_active')
    user_1_fixture.setdefault('user_position', 'manager')
    handler = UserHandler.from_user_id(id)
    handler.form = MultiDict(user_1_fixture)
    handler.form['email'] = new_email
    handler.form['last_name'] = new_last_name
    handler.update_current_user()
    user = User.query.filter_by(id=id).first()

    assert user == handler.user
    assert user.email == new_email
    assert handler.user.email == new_email
    assert handler.user.last_name == new_last_name

    create_test_user_position(user_positions_fixture['developer'])
    create_test_user(user_2_fixture)
    handler.form['email'] = user_2_fixture.get('email')
    handler.form['user_position'] = 'developer'
    with pytest.raises(UserAlreadyExists):
        handler.update_current_user()
