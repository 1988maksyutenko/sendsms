import flask


def get_admin_session_cookie(client: flask, email: str, password: str) -> str:
    response = client.post(
        '/admin',
        data={
            'email': email,
            'password': password
        }
    )
    cookies = response.headers.get('Set-Cookie').split(';')[0]

    return cookies
