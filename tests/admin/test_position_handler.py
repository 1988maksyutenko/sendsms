import pytest

from werkzeug.datastructures import MultiDict
from wtforms.validators import ValidationError

from tests.auth.fixtures import user_positions_fixture
from tests.auth.helpers import create_test_user_position

from app.auth.models import UserPosition
from app.admin.services import PositionHandler
from app.admin.services.errors import (
    UserPositionDoesNotExist,
    UserPositionAlreadyExist
)


def test_position_handler_initialization(
    client,
    user_positions_fixture
):
    position = create_test_user_position(user_positions_fixture.get('manager'))
    position_id = user_positions_fixture.get('manager').get('id')
    position_handler = PositionHandler.from_id(position_id)

    assert position_handler.position == position
    with pytest.raises(UserPositionDoesNotExist):
        PositionHandler.from_id(10000)

    u_position = create_test_user_position(
        user_positions_fixture.get('developer')
    )
    position = user_positions_fixture.get('developer')
    position.pop('id')
    form_data = MultiDict(position)
    position_handler = PositionHandler.from_form(form_data)
    assert u_position.name == position_handler.position.name
    assert u_position.description == position_handler.position.description

    invalid_form = MultiDict({
        'name': '',
        'desctipton': 'some description'
    })
    with pytest.raises(ValidationError):
        PositionHandler.from_form(invalid_form)


def test_position_handler_create_position(
    client,
    user_positions_fixture
):
    position = user_positions_fixture.get('manager')
    position.pop('id')
    form = MultiDict(position)
    handler = PositionHandler.from_form(form)
    new_position = handler.create_new_position()
    created_position = UserPosition.query.filter_by(
        name=position.get('name')
    ).first()
    assert isinstance(new_position, UserPosition)
    assert new_position == created_position

    create_test_user_position(user_positions_fixture.get('developer'))
    position = user_positions_fixture.get('developer')
    position.pop('id')
    form = MultiDict(position)
    handler = PositionHandler.from_form(form)
    with pytest.raises(UserPositionAlreadyExist):
        new_position = handler.create_new_position()


def test_position_handler_update_position(
    client,
    user_positions_fixture
):
    new_position_name = 'test position'
    new_positioni_description = 'new description'
    position = create_test_user_position(
        user_positions_fixture.get('manager')
    )
    form_data = user_positions_fixture.get('manager')
    form_data.pop('id')
    form_data['name'] = new_position_name
    form_data['description'] = new_positioni_description
    form = MultiDict(form_data)
    handler = PositionHandler.from_id(position.id)
    handler.form = form
    handler.update_current_position()
    assert handler.position.name == new_position_name
    assert handler.position.description == new_positioni_description

    create_test_user_position(user_positions_fixture.get('developer'))
    form_data = user_positions_fixture.get('developer')
    form_data.pop('id')
    form_data['name'] = user_positions_fixture.get('developer').get('name')
    form = MultiDict(form_data)
    handler = PositionHandler.from_id(position.id)
    handler.form = form
    with pytest.raises(UserPositionAlreadyExist):
        handler.update_current_position()


def test_upser_position_deletion(
    client,
    user_positions_fixture
):
    position = create_test_user_position(
        user_positions_fixture.get('manager')
    )
    handler = PositionHandler.from_id(position.id)
    handler.delete_position()

    assert not UserPosition.query.filter_by(id=position.id).first()
