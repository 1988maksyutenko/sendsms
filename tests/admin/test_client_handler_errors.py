from app.admin.services.errors import (
    ClientDoesNotExists,
    ClientAlreadyExists
)


def test_client_does_not_exists(client):
    error = ClientDoesNotExists()

    assert error.message == str(error)
    assert str(error) == repr(error)


def test_client_already_exists(client):
    error = ClientAlreadyExists()

    assert error.message == str(error)
    assert str(error) == repr(error)
