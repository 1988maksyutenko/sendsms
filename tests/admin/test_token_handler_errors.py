from app.admin.services.errors.token_handler_errors import TokenDoesNotExists
import pytest

from app.admin.services import TokenHandler
from app.admin.services.errors import TokenDoesNotExists


def test_token_does_not_exists(client):
    with pytest.raises(TokenDoesNotExists):
        TokenHandler.from_token_id(11)

    assert TokenDoesNotExists.message == str(TokenDoesNotExists())
    assert repr(TokenDoesNotExists()) == str(TokenDoesNotExists())
