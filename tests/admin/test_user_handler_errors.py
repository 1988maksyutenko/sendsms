from app.admin.services.errors import (
    UserAlreadyExists,
    UserDoesNotExists
)


def test_user_already_exists_error(client):
    err = UserAlreadyExists()

    assert str(err) == UserAlreadyExists.message
    assert repr(err) == str(err)


def test_user_does_not_exists_error(client):
    err = UserDoesNotExists()

    assert str(err) == UserDoesNotExists.message
    assert repr(err) == str(err)
