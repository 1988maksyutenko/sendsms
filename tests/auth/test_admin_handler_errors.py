from app.auth.services.errors import AdminAuthError


def test_admin_auth_error_str_method(client):
    message = 'test message'
    err = AdminAuthError(message)
    assert str(err) == message


def test_admin_auth_error_repr_method(client):
    message = 'test message'
    err = AdminAuthError(message)
    assert str(err) == err.__repr__()
    assert err.__repr__() == message
