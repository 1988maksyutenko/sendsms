from app.auth.models import (
    User,
    UserPosition,
    Client
)

from tests.constants import CLI_CLIENT_AUTHORIZATION_HEADERS
from tests.auth.helpers import (
    create_test_user,
    create_test_user_position,
    create_test_client
)
from tests.auth.fixtures import (
    admin_fixture,
    user_positions_fixture,
    cli_client_fixture
)


def test_get_token_200(
    client,
    cli_client_fixture
):
    create_test_client(cli_client_fixture)
    response = client.post(
        '/oauth/token',
        data={'grant_type': 'client_credentials'},
        headers=CLI_CLIENT_AUTHORIZATION_HEADERS
    )
    assert response.status == '200 OK'


def test_get_token_401(
    client,
    cli_client_fixture
):
    create_test_client(cli_client_fixture)
    response = client.post(
        '/oauth/token',
        data={'grant_type': 'client_credentials'},
        headers={'Authorization': 'Basic cXdlcjpxd2VyX3F3ZXI'}
    )
    assert response.status == '401 UNAUTHORIZED'


def test_admin_ok(client):
    response = client.get('/admin')
    assert response.status == '200 OK'


def test_admin_authorize_does_not_exists(
    client,
    admin_fixture
):
    response = client.post(
        '/admin',
        data={
            'email': admin_fixture['email'],
            'password': '1234'
        }
    )
    assert response.status == '400 BAD REQUEST'


def test_admin_authorize_success(
    client,
    admin_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    response = client.post(
        '/admin',
        data={
            'email': admin_fixture['email'],
            'password': admin_fixture['password'],
        }
    )
    assert response.status == '302 FOUND'


def test_admin_logout(
    client,
    admin_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture['admin'])
    create_test_user(admin_fixture)
    client.post(
        '/admin',
        data={
            'email': admin_fixture['email'],
            'password': admin_fixture['password'],
        }
    )

    response = client.get('/admin-logout')
    assert response.status == '200 OK'

    response = client.post('/admin-logout')
    assert response.status == '302 FOUND'
