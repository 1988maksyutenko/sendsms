import pytest
import time

from tests.constants import (
    CLI_CLIENT_ID,
    CLI_CLIENT_SECRET
)


@pytest.fixture
def admin_fixture():
    return {
        'id': 100000,
        'email': 'test-admin@mail.com',
        'password': '0p9o8i7u6y5t',
        'first_name': 'admin',
        'position_id': 100000,
        'is_active': True
    }


@pytest.fixture
def user_positions_fixture():
    return {
        'admin': {'name': 'admin', 'id': 100000},
        'manager': {'name': 'manager', 'id': 2},
        'developer': {'name': 'developer', 'id': 3}
    }


@pytest.fixture
def user_fixture():
    return {
        'id': 200000,
        'email': 'test-email@mail.com',
        'password': '0p9o8i7u6y5t',
        'first_name': 'user',
        'last_name': 'new-user',
        'middle_name': 'middle',
        'position_id': 2,
        'is_active': True
    }


@pytest.fixture
def cli_client_fixture():
    return {
        'name': 'cli',
        '_client_metadata': '{"token_endpoint_auth_method": "client_secret_basic", "grant_types": "client_credentials"}',  # noqa
        'client_id': CLI_CLIENT_ID,
        'client_secret': CLI_CLIENT_SECRET,
        'client_id_issued_at': 1,
        'client_secret_expires_at': 1,
        'id': 1
    }


@pytest.fixture
def token_fixture():
    return {
        'client_id': 'qwer',
        'token_type': 'Bearer',
        'access_token': 'GK5lyYKZNfqtfxYYBFUqKJygpTJEhjq7jdEaTmjL0jo61stbVTJpZ6SZ59DPlCLExbdpD9GsP7lPFH8a0lAolMs',  # noqa
        'issued_at': time.time(),
        'access_token_revoked_at': 0,
        'refresh_token_revoked_at': 0,
        'expires_in': 1,
        'id': 1
    }


@pytest.fixture
def auth_code_fixture():
    return {
        'id': 1,
        'code': 'JlkdjlSlkj23L:Jj434545',
        'client_id': 'qwer',
        'auth_time': 12312352
    }
