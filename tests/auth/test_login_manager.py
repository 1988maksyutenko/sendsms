from app.auth.login_manager import load_user
from tests.auth.fixtures import (
    user_fixture,
    user_positions_fixture
)
from tests.auth.helpers import (
    create_test_user,
    create_test_user_position
)


def test_load_user(
    client,
    user_fixture,
    user_positions_fixture
):
    user = load_user(user_fixture.get('id'))
    assert not user

    create_test_user_position(user_positions_fixture['manager'])
    create_test_user(user_fixture)
    user = load_user(user_fixture.get('id'))
    assert user
