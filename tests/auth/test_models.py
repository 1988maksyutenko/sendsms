from time import sleep

from app.app import db

from tests.admin.fixtures import user_1_fixture
from tests.auth.fixtures import (
    admin_fixture,
    user_fixture,
    user_positions_fixture,
    cli_client_fixture,
    token_fixture,
    auth_code_fixture
)
from tests.auth.helpers import (
    create_test_user_position,
    create_test_user,
    create_test_client,
    create_test_token,
    create_test_auth_code
)


def test_user_position_model(
    client,
    user_positions_fixture
):
    position = user_positions_fixture['admin']
    user_position = create_test_user_position(position)
    db.session.add(user_position)
    db.session.commit()

    assert repr(user_position) == f'User position is: {position["name"]}'
    assert str(user_position) == position['name']


def test_user_is_admin(
    client,
    admin_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture['admin'])
    user = create_test_user(admin_fixture)

    assert user.is_admin()


def test_user_model(
    client,
    user_fixture,
    user_1_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture['manager'])
    user = create_test_user(user_fixture)
    user_1 = create_test_user(user_1_fixture)
    assert repr(user) == 'User fullname: ' + \
        f'{user_fixture["last_name"]} {user_fixture["first_name"]} ' + \
        f'{user_fixture["middle_name"]} ' + \
        f'User ID: {user_fixture["id"]}'

    assert str(user) == user_fixture['last_name'] + ' ' + \
        user_fixture['first_name']

    assert user.get_user_id() == user_fixture['id']
    assert user != user_1


def test_client_model(
    client,
    cli_client_fixture
):
    c_client = create_test_client(cli_client_fixture)
    assert repr(c_client) == cli_client_fixture['name']
    assert str(c_client) == repr(c_client)


def test_token_model(
    client,
    cli_client_fixture,
    token_fixture
):
    create_test_client(cli_client_fixture)
    token = create_test_token(token_fixture)
    assert str(token) == token_fixture['access_token']
    assert repr(token) == str(token)


def test_auth_code_model(
    client,
    auth_code_fixture
):
    code = create_test_auth_code(auth_code_fixture)
    assert str(code) == auth_code_fixture['code']
    assert repr(code) == str(code)


def test_client_is_refresh_token_valid(
    client,
    token_fixture,
    cli_client_fixture
):
    create_test_client(cli_client_fixture)
    token = create_test_token(token_fixture)
    assert token.is_refresh_token_valid()
    sleep(2)
    assert not token.is_refresh_token_valid()
