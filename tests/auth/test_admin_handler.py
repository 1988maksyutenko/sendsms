import pytest

from app.auth.services import AdminAuthHandler
from app.auth.services.errors import AdminAuthError
from tests.auth.fixtures import (
    admin_fixture,
    user_positions_fixture
)
from tests.auth.helpers import (
    create_test_user_position,
    create_test_user
)


def test_admin_log_in_fail(
    client,
):
    client.form = {}
    client.post('/admin')
    handler = AdminAuthHandler(client)
    with pytest.raises(AdminAuthError):
        handler.admin_login()


def test_admin_log_in_success(
    client,
    admin_fixture,
    user_positions_fixture
):
    create_test_user_position(user_positions_fixture.get('admin'))
    create_test_user(admin_fixture)

    client.form = {
        'email': admin_fixture['email'],
        'password': admin_fixture['password']
    }
    client.post('/admin')
    handler = AdminAuthHandler(client)
    handler.admin_login()


def test_str_method(client):
    client.form = {}
    client.post('/admin')
    handler = AdminAuthHandler(client)
    assert str(handler) == 'Admin authentication and authorization'


def test_repr_method(client):
    client.form = {}
    client.post('/admin')
    handler = AdminAuthHandler(client)
    assert str(handler) == handler.__repr__()

