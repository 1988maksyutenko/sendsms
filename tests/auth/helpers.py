from app.app import db

from app.auth.models import (
    User,
    UserPosition,
    Client,
    Token,
    AuthorizationCode
)


def create_test_user_position(position: dict) -> UserPosition:
    """Creates the test user position."""
    _position = UserPosition(**position)
    db.session.add(_position)
    db.session.commit()
    return _position


def create_test_user(user: dict) -> User:
    """Creates the test user."""
    _user = User(**user)
    db.session.add(_user)
    db.session.commit()
    return _user


def create_test_client(client: Client) -> Client:
    """Creates teh test client."""
    _client = Client(**client)
    db.session.add(_client)
    db.session.commit()
    return _client


def create_test_token(token: dict) -> Token:
    """Creates the test token."""
    _token = Token(**token)
    db.session.add(_token)
    db.session.commit()
    return _token


def create_test_auth_code(code: dict) -> AuthorizationCode:
    """Creates teh test authorization code."""
    _code = AuthorizationCode(**code)
    db.session.add(_code)
    db.session.commit()
    return _code
