import pytest

from app.app import (
    create_app,
    db,
    login_manager
)


@pytest.fixture
def client():
    import app.sms.views

    app = create_app('test')
    app.config.from_object('settings.test.config')

    db.init_app(app)
    login_manager.init_app(app)

    with app.test_client() as client:
        with app.app_context():
            db.create_all()
            yield client
            db.session.remove()
            db.drop_all()
