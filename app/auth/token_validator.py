from authlib.integrations.flask_oauth2 import ResourceProtector
from authlib.oauth2.rfc6750 import BearerTokenValidator

from app.auth.models import Token


class NewBearerTokenValidator(BearerTokenValidator):
    def authenticate_token(self, token_string):
        return Token.query.filter_by(access_token=token_string).first()


require_oauth = ResourceProtector()
require_oauth.register_token_validator(NewBearerTokenValidator())
