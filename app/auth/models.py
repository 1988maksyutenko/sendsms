from authlib.integrations.sqla_oauth2 import OAuth2ClientMixin
from authlib.integrations.sqla_oauth2 import OAuth2TokenMixin
from authlib.integrations.sqla_oauth2 import OAuth2AuthorizationCodeMixin
from flask_login import UserMixin

from app.app import db


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), nullable=False, unique=True)
    password = db.Column(db.String(64), nullable=False)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    middle_name = db.Column(db.String(64))
    is_active = db.Column(db.Boolean())
    position_id = db.Column(db.Integer, db.ForeignKey('user_position.id'))
    user_position = db.relationship(
        'UserPosition',
        backref=db.backref('user', lazy=True)
    )

    def __str__(self) -> str:
        last_name = self.last_name if self.last_name else ''
        first_name = self.first_name if self.first_name else ''
        return last_name + ' ' + first_name

    def __repr__(self) -> str:
        last_name = self.last_name if self.last_name else ''
        first_name = self.first_name if self.first_name else ''
        middle_name = self.middle_name if self.middle_name else ''
        return 'User fullname: ' + \
            f'{last_name} {first_name} {middle_name} ' + \
            f'User ID: {self.id}'

    def __eq__(self, other):
        return self.email == other.email

    def get_user_id(self) -> int:
        return self.id

    def is_admin(self) -> bool:
        return self.user_position.name == 'admin'


class UserPosition(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False, unique=True)
    description = db.Column(db.String(512))

    def __repr__(self) -> str:
        return f'User position is: {self.name}'

    def __str__(self) -> str:
        return self.name


class Client(db.Model, OAuth2ClientMixin):
    client_id = db.Column(db.String(48), index=True, unique=True)
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), nullable=False)
    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id')
    )
    user = db.relationship('User')

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return self.__str__()

    def __eq__(self, __o: object) -> bool:
        return self.client_id == __o.client_id


class Token(db.Model, OAuth2TokenMixin):
    client_id = db.Column(db.String(48), db.ForeignKey('client.client_id'))
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id', ondelete='CASCADE')
    )
    user = db.relationship('User')
    client = db.relationship('Client')

    def __str__(self) -> str:
        return self.access_token

    def __repr__(self) -> str:
        return self.__str__()

    def is_refresh_token_valid(self):
        return not self.is_revoked() and not self.is_expired()


class AuthorizationCode(db.Model, OAuth2AuthorizationCodeMixin):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id', ondelete='CASCADE')
    )
    user = db.relationship('User')

    def __str__(self) -> str:
        return self.code

    def __repr__(self) -> str:
        return self.__str__()
