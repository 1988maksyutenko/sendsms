from flask import Request

from flask_login import login_user
from flask_login.utils import logout_user

from app.auth.models import User
from app.auth.services.errors import AdminAuthError


__all__ = [
    'AdminAuthHandler',
]


class AdminAuthHandler:
    """The class wraps the admin authentication and authorization."""

    def __init__(self, request: Request) -> None:
        self._request = request

    def __str__(self) -> str:
        return 'Admin authentication and authorization'

    def __repr__(self) -> str:
        return self.__str__()

    def admin_login(self) -> None:
        password = self._request.form.get('password', None)
        email = self._request.form.get('email', None)
        user = User.query.filter_by(email=email, password=password).first()
        if user is not None and user.is_admin():
            login_user(user)
        else:
            raise AdminAuthError('Admin does not exist')

    def admin_logout(self) -> None:
        logout_user()
