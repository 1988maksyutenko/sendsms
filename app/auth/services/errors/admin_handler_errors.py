__all__ = [
    'AdminAuthError'
]


class AdminAuthError(Exception):
    """
    The generic class.
    Represents the exception for the admin authentication and authorization.
    """
    def __init__(self, message: str) -> None:
        self._message = message

    def __str__(self) -> str:
        return self._message

    def __repr__(self) -> str:
        return self.__str__()
