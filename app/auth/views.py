from flask import (
    Blueprint,
    request,
    render_template,
    redirect
)

from flask_login.utils import login_required

from app.app import (
    server,
    csrf
)
from app.auth.services import AdminAuthHandler
from app.auth.services.errors import AdminAuthError


auth_blueprint = Blueprint('auth', __name__)


@auth_blueprint.route('/oauth/token', methods=['POST'])
@csrf.exempt
def issue_token():
    return server.create_token_response(request)


@auth_blueprint.route('/admin', methods=['GET', 'POST'])
def admin():
    if request.method == 'GET':
        return render_template('admin-login.html', data={})
    if request.method == 'POST':
        admin_handler = AdminAuthHandler(request)
        try:
            admin_handler.admin_login()
        except AdminAuthError:
            return render_template(
                'admin-login.html',
                data={'message': 'invalid email or password.'}
            ), 400
        return redirect('/admin-page')


@auth_blueprint.route('/admin-logout', methods=['GET', 'POST'])
@login_required
def admin_log_out():
    if request.method == 'GET':
        return render_template('admin-logout.html')
    if request.method == 'POST':
        admin_handler = AdminAuthHandler(request)
        admin_handler.admin_logout()
        return redirect('/admin')
