from typing import Union

from app.app import login_manager
from app.auth.models import User


@login_manager.user_loader
def load_user(user_id: str) -> Union[None, User]:
    user = User.query.filter_by(id=int(user_id)).first()
    if user is None:
        return
    return user
