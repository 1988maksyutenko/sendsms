from flask import request
from flask_restful import Resource
from flask import Blueprint

from app.app import api

from app.auth.token_validator import require_oauth


class SmsView(Resource):

    @require_oauth()
    def get(self):
        request_metshod = request.method
        return {
            'sms': 'test message.',
            'method': request_metshod,
        }

    @require_oauth()
    def post(self):
        sms = request.json
        return sms


api.add_resource(SmsView, '/api/v1/sms')
api_bp = Blueprint('api', __name__)

