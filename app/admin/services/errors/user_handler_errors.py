__all__ = [
    'UserAlreadyExists',
    'UserDoesNotExists'
]


class UserAlreadyExists(Exception):
    message = 'User already exists.'

    def __str__(self) -> str:
        return self.message

    def __repr__(self) -> str:
        return self.__str__()


class UserDoesNotExists(Exception):
    message = 'User does not exists.'

    def __str__(self) -> str:
        return self.message

    def __repr__(self) -> str:
        return self.__str__()
