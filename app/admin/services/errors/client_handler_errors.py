class ClientDoesNotExists(Exception):
    message = 'Client does not exists.'

    def __str__(self) -> str:
        return self.message

    def __repr__(self) -> str:
        return self.__str__()


class ClientAlreadyExists(Exception):
    message = 'Client already exists.'

    def __str__(self) -> str:
        return self.message

    def __repr__(self) -> str:
        return self.__str__()
