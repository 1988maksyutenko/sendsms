__all__ = [
    'TokenDoesNotExists'
]


class TokenDoesNotExists(Exception):
    message = 'Token does not exists.'

    def __str__(self) -> str:
        return self.message

    def __repr__(self) -> str:
        return self.__str__()
