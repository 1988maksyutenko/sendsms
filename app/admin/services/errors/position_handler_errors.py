class UserPositionDoesNotExist(Exception):
    message = 'User position does not exist.'

    def __str__(self) -> str:
        return self.message

    def __repr__(self) -> str:
        return self.__str__()


class UserPositionAlreadyExist(Exception):
    message = 'User position already exist.'

    def __str__(self) -> str:
        return self.message

    def __repr__(self) -> str:
        return self.__str__()
