from app.app import db
from app.auth.models import (
    Token
)
from app.admin.services.errors import TokenDoesNotExists


class TokenHandler:
    """Manage the operations with tokens."""

    def __init__(self, token: Token):
        self._token = token

    @property
    def token(self) -> Token:
        return self._token

    @classmethod
    def from_token_id(cls, id: int) -> 'TokenHandler':
        token = Token.query.filter_by(id=id).first()
        if token is None:
            raise TokenDoesNotExists()
        return cls(token)

    def delete_token(self) -> Token:
        db.session.delete(self._token)
        db.session.commit()
        return self._token
