from sqlalchemy.exc import IntegrityError
from werkzeug.datastructures import MultiDict
from wtforms import (
    Form,
    StringField,
    TextAreaField,
    IntegerField
)
from wtforms.validators import (
    Length,
    DataRequired,
    ValidationError
)

from app.app import db
from app.auth.models import Client
from app.admin.services.errors import (
    ClientDoesNotExists,
    ClientAlreadyExists
)


class ClientForm(Form):
    client_id = StringField(
        'Client ID',
        validators=[Length(max=48), DataRequired()]
    )
    client_secret = StringField(
        'Client Secret',
        validators=[Length(max=120), DataRequired()]
    )
    client_metadata = TextAreaField('Clent Metadata')
    name = StringField('Name', validators=[Length(max=32)])
    user_id = IntegerField('User ID', validators=[DataRequired()])

    def to_dict(self) -> dict:
        return {k: v.data for k, v, in self._fields.items()}


class ClientHandler:
    """
    Manages the operations wht the client.
    """

    def __init__(self, client: Client) -> None:
        self._client = client
        self._form = None

    @property
    def client(self) -> Client:
        return self._client

    @property
    def form(self) -> Form:
        return self._form

    @form.setter
    def form(self, new_form):
        form = ClientForm(new_form)
        if form.validate():
            self._form = form.to_dict()
        else:
            raise ValidationError(form.errors)

    @classmethod
    def from_client_id(cls, id: int) -> 'ClientHandler':
        obj = Client.query.filter_by(id=id).first()
        if obj is None:
            raise ClientDoesNotExists()
        return cls(obj)

    @classmethod
    def from_form(cls, new_form) -> 'ClientHandler':
        new_form = MultiDict(dict(new_form))
        metadata = new_form.get('_client_metadata')
        new_form.setdefault('client_metadata', metadata)
        form = ClientForm(new_form)
        if form.validate():
            client = Client(
                client_id=form.client_id.data,
                client_secret=form.client_secret.data,
                _client_metadata=form.client_metadata.data,
                name=form.name.data,
                user_id=form.user_id.data
            )
        else:
            raise ValidationError(form.errors)
        obj = cls(client)
        obj._form = form.to_dict()
        return obj

    def update_client(self) -> Client:
        """Updates the client."""
        metadata = self._form.pop('client_metadata')
        self._form.setdefault('_client_metadata', metadata)
        db.session.query(
            Client
        ).filter(
            Client.client_id == self._client.client_id
        ).update(
            self.form,
            synchronize_session=False
        )
        db.session.commit()
        return self._client

    def create_new_client(self) -> Client:
        """The new client creation."""
        db.session.add(self._client)
        try:
            db.session.commit()
        except IntegrityError:
            raise ClientAlreadyExists()
        return self._client

    def delete_client(self) -> Client:
        """The client deletion."""
        db.session.delete(self._client)
        db.session.commit()
        return self._client
