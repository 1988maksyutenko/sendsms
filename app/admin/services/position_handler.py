from sqlalchemy.exc import IntegrityError
from wtforms import (
    Form,
    StringField,
)
from wtforms.validators import (
    DataRequired,
    Length,
    ValidationError,
    DataRequired
)

from app.app import db
from app.auth.models import UserPosition
from app.admin.services.errors import (
    UserPositionDoesNotExist,
    UserPositionAlreadyExist
)


class PositionForm(Form):
    name = StringField(
        'Name field',
        validators=[DataRequired(), Length(max=64)]
    )
    description = StringField(
        'Description field',
        validators=[Length(max=512)]
    )


class PositionHandler:
    """Manage the operations with the user positions"""

    def __init__(self, position: UserPosition) -> None:
        self._position = position
        self._form = None

    @property
    def form(self):
        return self._form

    @form.setter
    def form(self, form):
        self._form = form

    @property
    def position(self) -> UserPosition:
        return self._position

    @classmethod
    def from_id(cls, id: int) -> 'PositionHandler':
        u_position = UserPosition.query.filter_by(id=id).first()
        if u_position is None:
            raise UserPositionDoesNotExist()
        return cls(u_position)

    @classmethod
    def from_form(cls, form) -> 'PositionHandler':
        form = PositionForm(form)
        if not form.validate():
            raise ValidationError(form.errors)

        u_position = UserPosition(
            name=form.name.data,
            description=form.description.data
        )
        obj = cls(u_position)
        obj._form = form
        return obj

    def create_new_position(self) -> UserPosition:
        db.session.add(self._position)
        try:
            db.session.commit()
        except IntegrityError:
            raise UserPositionAlreadyExist()
        return self._position

    def delete_position(self) -> UserPosition:
        db.session.delete(self._position)
        db.session.commit()
        return self._position

    def update_current_position(self) -> UserPosition:
        try:
            db.session.query(
                UserPosition
            ).filter(
                UserPosition.id == self.position.id
            ).update(
                self.form,
                synchronize_session=False
            )
            db.session.commit()
        except IntegrityError:
            raise UserPositionAlreadyExist()
        else:
            return self._position
