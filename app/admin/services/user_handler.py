from sqlalchemy.exc import IntegrityError
from wtforms import (
    Form,
    StringField,
    PasswordField,
    EmailField
)
from wtforms.form import FormMeta
from wtforms.validators import (
    DataRequired,
    Email,
    Length,
    ValidationError
)

from app.app import db
from app.auth.models import (
    User,
    UserPosition
)
from app.admin.services.errors import (
    UserAlreadyExists,
    UserDoesNotExists
)


class UserForm(Form):
    email = EmailField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    first_name = StringField('First name', validators=[Length(max=64)])
    last_name = StringField('Last name', validators=[Length(max=64)])
    middle_name = StringField('Middle name', validators=[Length(max=64)])
    user_position = StringField('User positon', validators=[Length(max=64)])

    def to_dict(self) -> dict:
        return {k: v.data for k, v, in self._fields.items()}


class UserHandler:
    """Manage the operations with the users."""

    def __init__(self, user: User = None):
        self._user = user
        self._form = None

    @property
    def user(self):
        return self._user

    @property
    def form(self):
        return self._form

    @form.setter
    def form(self, new_form):
        form = UserForm(new_form)
        if form.validate():
            self._form = form.to_dict()
        else:
            raise ValidationError(form.errors)

    @classmethod
    def from_user_id(cls, user_id: int) -> 'UserHandler':
        user = User.query.filter_by(id=user_id).first()
        if user is None:
            raise UserDoesNotExists()
        else:
            obj = cls(user)
            obj._user = user
            return obj

    @classmethod
    def from_form(cls, form) -> 'UserHandler':
        form = UserForm(form)
        try:
            u_position = UserPosition.query.filter_by(
                name=form.user_position.data
            ).first().id
        except AttributeError:
            raise ValidationError(form.errors)
        if form.validate():
            user = User(
                email=form.email.data,
                password=form.password.data,
                first_name=form.first_name.data,
                last_name=form.last_name.data,
                middle_name=form.middle_name.data,
                position_id=int(u_position),
                is_active=True
            )
        else:
            raise ValidationError(form.errors)
        obj = cls(user)
        obj._form = form.to_dict()
        return obj

    def create_new_user(self) -> User:
        db.session.add(self._user)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.remove()
            raise UserAlreadyExists()
        else:
            return self._user

    def delete_user(self) -> User:
        db.session.delete(self._user)
        db.session.commit()
        return self._user

    def get_current_user(self) -> User:
        return self.user

    def update_current_user(self) -> User:
        u_position = UserPosition.query.filter_by(
            name=self.form.get('user_position')
        ).first().id
        self.form.pop('user_position')
        self.form['position_id'] = int(u_position)
        try:
            db.session.query(
                User
            ).filter(
                User.id == self.user.id
            ).update(
                self.form,
                synchronize_session=False
            )
            db.session.commit()
        except IntegrityError:
            db.session.commit()
            raise UserAlreadyExists()
        else:
            return self._user
