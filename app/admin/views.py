from crypt import methods
from app.admin.services.errors.client_handler_errors import ClientDoesNotExists
from flask import (
    Blueprint,
    render_template,
    request
)
from flask_login import login_required

from app.admin.services.position_handler import PositionHandler
from app.admin.services.errors import (
    UserDoesNotExists,
    UserPositionDoesNotExist
)
from app.auth.models import (
    User,
    UserPosition,
    Token,
    Client
)
from app.admin.services import (
    UserHandler,
    UserForm,
    TokenHandler,
    ClientHandler
)
from app.admin.services.errors import (
    UserAlreadyExists,
    UserPositionAlreadyExist,
    ClientAlreadyExists
)
from wtforms.validators import ValidationError

admin_blueprint = Blueprint('admin', __name__)


@admin_blueprint.route('/admin-page', methods=['GET'])
@login_required
def admin_page():
    return render_template('admin-page.html')


@admin_blueprint.route('/create-user', methods=['GET', 'POST'])
@login_required
def user_creation():
    if request.method == 'GET':
        positions = UserPosition.query.all()
        return render_template('create-user.html', u_positions=positions)
    if request.method == 'POST':
        try:
            handler = UserHandler.from_form(request.form)
            handler.create_new_user()
        except (
            UserAlreadyExists,
            ValidationError
        ) as err:
            positions = UserPosition.query.all()
            return render_template(
                'create-user.html',
                data={'message': str(err)},
                user=handler.user,
                u_positions=positions
            ), 400
        else:
            return render_template(
                'admin-page.html',
                data={'message': f'created {str(handler.user)}'}
            ), 201


@admin_blueprint.route('/delete-user/<int:id>', methods=['POST'])
@login_required
def user_deletion(id):
    try:
        handler = UserHandler.from_user_id(id)
    except UserDoesNotExists as err:
        return render_template(
            'admin-page.html',
            data={'message': str(err)}
        ), 400
    else:
        deleted_user = handler.delete_user()
        return render_template(
            'admin-page.html',
            data={'message': f'deleted {str(deleted_user)}'}
        )


@admin_blueprint.route('/list-users', methods=['GET'])
@login_required
def list_users():
    users = User.query.order_by(User.last_name).all()
    return render_template('list-users.html', data=users), 200


@admin_blueprint.route('/user/<int:id>', methods=['GET', 'POST'])
@login_required
def user_set(id):
    if request.method == 'GET':
        positions = UserPosition.query.all()
        form = UserForm()
        handler = UserHandler.from_user_id(id)
        user = handler.get_current_user()
        return render_template(
            'user.html',
            u_positions=positions,
            user=user,
            form=form
        )
    if request.method == 'POST':
        handler = UserHandler.from_user_id(id)
        handler.form = request.form
        try:
            user = handler.update_current_user()
        except UserAlreadyExists as err:
            return render_template(
                'user.html',
                data={'message': str(err)},
                user=handler.user
            ), 400
        message_data = {'message': f'User {str(user)} updated!'}
        return render_template('admin-page.html', data=message_data, user=user)


@admin_blueprint.route('/list-positions', methods=['GET'])
@login_required
def list_positions():
    positions = UserPosition.query.order_by(UserPosition.name).all()
    return render_template('list-positions.html', data=positions)


@admin_blueprint.route('/create-position', methods=['GET', 'POST'])
@login_required
def create_position():
    if request.method == 'GET':
        return render_template('create-position.html')
    if request.method == 'POST':
        try:
            handler = PositionHandler.from_form(request.form)
            new_position = handler.create_new_position()
        except UserPositionAlreadyExist as err:
            return render_template(
                'create-position.html',
                data={'message': str(err)},
                position=handler.position
            ), 400
        else:
            return render_template(
                'admin-page.html',
                data={'message': f'created {str(new_position)} position'}
            ), 201


@admin_blueprint.route('/position/<int:id>', methods=['GET', 'POST'])
@login_required
def position_set(id):
    if request.method == 'GET':
        position = UserPosition.query.filter_by(id=id).first()
        return render_template('position.html', position=position)
    if request.method == 'POST':
        handler = PositionHandler.from_id(id)
        handler.form = request.form
        try:
            handler.update_current_position()
        except UserPositionAlreadyExist as err:
            return render_template(
                'position.html',
                data={'message': str(err)},
                position=handler.position
            ), 400
        message_data = {'message': f'Position {handler.position} updated!'}
        return render_template(
            'admin-page.html',
            data=message_data,
            position=handler.position
        )


@admin_blueprint.route('/delete-position/<int:id>', methods=['POST'])
@login_required
def delete_position(id):
    try:
        handler = PositionHandler.from_id(id)
    except UserPositionDoesNotExist as err:
        return render_template(
            'admin-page.html',
            data=str(err)
        ), 400
    position = handler.delete_position()
    return render_template(
        'admin-page.html',
        data={'message': f'deleted {str(position)}'}
    )


@admin_blueprint.route('/tokens', methods=['GET'])
@login_required
def get_all_tokens():
    tokens = Token.query.all()
    return render_template(
        'list-tokens.html',
        tokens=tokens
    )


@admin_blueprint.route('/token/<int:id>', methods=['GET', 'POST'])
@login_required
def token_management(id):
    handler = TokenHandler.from_token_id(id)
    if request.method == 'GET':
        return render_template(
            'token.html',
            token=handler.token
        )
    if request.method == 'POST':
        token = handler.delete_token()
        return render_template(
            'admin-page.html',
            data={'message': f'token {token.access_token} was deleted.'}
        )


@admin_blueprint.route('/clients', methods=['GET'])
@login_required
def get_all_clients():
    clients = Client.query.all()
    return render_template(
        'list-clients.html',
        clients=clients
    )


@admin_blueprint.route('/client/<int:id>', methods=['GET', 'POST'])
@login_required
def client_management(id):
    handler = ClientHandler.from_client_id(id)
    if request.method == 'GET':
        return render_template(
            'client.html',
            client=handler.client
        )
    if request.method == 'POST':
        handler.form = request.form
        handler.update_client()
        return render_template(
            'admin-page.html',
            data={'message': 'client was updated'}
        )


@admin_blueprint.route('/create-client', methods=['GET', 'POST'])
@login_required
def create_new_client():
    if request.method == 'GET':
        return render_template('create-client.html')
    if request.method == 'POST':
        try:
            handler = ClientHandler.from_form(request.form)
            handler.create_new_client()
        except ClientAlreadyExists as err:
            return render_template(
                'create-client.html',
                data={'message': str(err)},
                client=handler.client,
            ), 400
        except ValidationError as err:
            return render_template(
                'create-client.html',
                data={'message': str(err)},
            ), 400
        else:
            return render_template(
                'admin-page.html',
                data={'message': f'created {str(handler.client)}'}
            ), 201


@admin_blueprint.route('/delete-client/<int:id>', methods=['POST'])
@login_required
def delete_client(id):
    try:
        handler = ClientHandler.from_client_id(id)
        handler.delete_client()
    except ClientDoesNotExists as err:
        return render_template(
            'admin-page.html',
            data={'message': str(err)}
        ), 400
    else:
        return render_template(
            'admin-page.html',
            data={'message': f'deleted: {handler.client}'}
        )
