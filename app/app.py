from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
from flask_wtf.csrf import CSRFProtect
from flask_login import (
    LoginManager,
    FlaskLoginClient
)

from authlib.integrations.flask_oauth2 import AuthorizationServer
from authlib.integrations.sqla_oauth2 import (
    create_query_client_func,
    create_save_token_func
)
from authlib.oauth2.rfc6749 import grants


api = Api()
db = SQLAlchemy()
migrate = Migrate()
server = AuthorizationServer()
login_manager = LoginManager()
csrf = CSRFProtect()


def create_app(dir: str = None) -> Flask:
    from app.auth.views import auth_blueprint
    from app.admin.views import admin_blueprint
    from app.sms.views import api_bp
    from app.auth.models import Client, Token

    import app.sms.views

    import app.auth.models

    import app.auth.login_manager


    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(f'settings.{dir}.config')
    app.test_client_class = FlaskLoginClient

    csrf.init_app(app)
    csrf.exempt(api_bp)
    db.init_app(app)
    migrate.init_app(app, db)
    api.init_app(api_bp)
    server.init_app(
        app,
        query_client=create_query_client_func(db.session, Client),
        save_token=create_save_token_func(db.session, Token)
    )
    login_manager.init_app(app)
    server.register_grant(grants.ClientCredentialsGrant)

    app.register_blueprint(auth_blueprint)
    app.register_blueprint(admin_blueprint)
    app.register_blueprint(api_bp)
    CORS(app)


    return app

