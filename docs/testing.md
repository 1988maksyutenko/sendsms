# All commands must be executed from <mark style="color:red; background:white">project root</mark> directory.

### <mark style="color:red; background:white">Start</mark> the docker test containers before tests run: 

- `docker-compose -f settings/test/test-compose.yaml up`

### <mark style="color:red; background:white">Stop</mark> the docker test containers after tests:

- `docker-compose -f settings/test/test-compose.yaml down`

| Actions                                             | Commands                                               |
| --------------------------------------------------- | ------------------------------------------------------ |
| Run all tests                                       | `coverage run --source=app -m pytest -v ./tests`       |
| Run tests for the sms package                       | `coverage run --source=app -m pytest -v ./tests/sms`   |
| Run tests for the auth package                      | `coverage run --source=app -m pytest -v ./tests/auth`  |
| Run tests for the admin package                     | `coverage run --source=app -m pytest -v ./tests/admin` |
| Generate report                                     | `coverage report`                                      |
| Generate html page with report (htmlcov/index.html) | `coverage html`                                        |

