# The administrative panel for the authentication management.

#### Path: {scheme}://{domain}:{port}/admin

#### Sections:

1. Users
2. User Positions
3. Tokens
4. Clients