import os

from settings.common_config import *  # noqa


DEBUG = True

SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI', None)

OAUTH2_TOKEN_EXPIRES_IN = {
    'authorization_code': 20,
    'implicit': 20,
    'password': 20,
    'client_credentials': 20
}

CORS_ALLOW_HEADERS = '0.0.0.0'
