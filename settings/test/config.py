from settings.common_config import *  # noqa

ENV = 'test'

DEBUG = True

SQLALCHEMY_DATABASE_URI = 'postgresql://user:password@0.0.0.0:5433/test_sms_db'

OAUTH2_TOKEN_EXPIRES_IN = {
    'authorization_code': 1,
    'implicit': 1,
    'password': 1,
    'client_credentials': 1
}

WTF_CSRF_CHECK_DEFAULT = False
