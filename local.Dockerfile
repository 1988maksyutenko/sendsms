FROM python:3.10

RUN pip install pdm

WORKDIR /application

ENV PATH=$PATH:/application/__pypackages__/3.9/bin
ENV PYTHONPATH=/application/__pypackages__/3.9/lib \
    PYTHONFAULTHANDLER=1 \ 
	PYTHONHASHSEED=random 
ENV FLASK_APP=run:app

COPY . .

RUN pdm install