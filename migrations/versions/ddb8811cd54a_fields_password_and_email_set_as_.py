"""Fields password and email set as nullable.

Revision ID: ddb8811cd54a
Revises: 088c7433a200
Create Date: 2022-05-11 18:07:09.842523

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ddb8811cd54a'
down_revision = '088c7433a200'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('user', 'email',
               existing_type=sa.VARCHAR(length=64),
               nullable=False)
    op.alter_column('user', 'password',
               existing_type=sa.VARCHAR(length=64),
               nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('user', 'password',
               existing_type=sa.VARCHAR(length=64),
               nullable=True)
    op.alter_column('user', 'email',
               existing_type=sa.VARCHAR(length=64),
               nullable=True)
    # ### end Alembic commands ###
